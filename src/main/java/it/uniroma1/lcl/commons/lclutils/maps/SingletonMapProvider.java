package it.uniroma1.lcl.commons.lclutils.maps;

import java.io.IOException;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by tommaso on 01/12/16.
 */
public class SingletonMapProvider
{
	private static Map<String, Map> singletons;

	public static String FILE_PATH;
	public static Function keyTransformer;
	public static Function valueTransformer;


	public static <K, V> Map<K, V> getInstance(String key)
	{
		if(!singletons.containsKey(key))
		{
			if(FILE_PATH != null && keyTransformer != null && valueTransformer != null)
			{
				try
				{
					Map map = MapUtils.readMap(FILE_PATH, "\t", keyTransformer, valueTransformer);
					singletons.put(key, map);
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return singletons.get(key);
	}
}
