package it.uniroma1.lcl.commons.lclutils.misc;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by tommaso on 07/10/16.
 */
public class MiscUtils
{

	public static String decorateString(String toDecorate, char decoration, int repeat)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < repeat; i++)
			builder.append(decoration);
		builder.append(" ").append(toDecorate).append(" ");
		for (int i = 0; i < repeat; i++)
			builder.append(decoration);
		return builder.toString();
	}

	public static boolean isOnlyAlpha(String str)
	{
		for (int i = 0; i < str.length(); i++)
		{
			if (!Character.isAlphabetic(str.charAt(i)))
				return false;
		}
		return true;
	}

	public static void main(String[] args)
	{
		Path path = Paths.get("/media/tommaso/4940d845-c3f3-4f0b-8985-f91a0b453b07/itwac-postag/ITWAC-11.xml");
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile()), Charset.forName("ISO-8859-1"))))
		{
			int count = 0;
			while(reader.ready())
			{
				System.out.println(reader.readLine());
				if(count++ >= 30)
					break;
			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}
}
