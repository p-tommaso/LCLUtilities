package it.uniroma1.lcl.commons.lclutils.nasari;

import it.uniroma1.lcl.jlt.util.Pair;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tommaso on 03/10/16.
 */
public class NasariVector<T>
{
	private static class NasariComponent<T>
	{
		private T id;
		private Double value;

		public NasariComponent(T id)
		{
			this.id = id;
			value = null;
		}

		public T getId()
		{
			return id;
		}

		public double getValue()
		{
			return value;
		}
	}

	private List<NasariComponent<T>> vector;

	public NasariVector()
	{
		this.vector = new LinkedList<>();
	}

	public List<NasariComponent<T>> getVector()
	{
		return vector;
	}

	public T getComponent(int i)
	{
		return vector.get(i).getId();
	}

	public static <T> NasariVector<T> simpleVectorFromList(List<T> components)
	{
		NasariVector<T> v = new NasariVector<>();
		for(T t : components)
		{
			v.vector.add(new NasariComponent<>(t));
		}
		return v;
	}

	public static <T> NasariVector<T> valuedVectorFromPairs(List<Pair<T, Double>> compoenents)
	{
		NasariVector<T> v = new NasariVector<>();
		for(Pair<T, Double> pair : compoenents)
		{
			NasariComponent<T> nasariComponent = new NasariComponent<>(pair.getFirst());
			nasariComponent.value = pair.getSecond();
			v.vector.add(nasariComponent);
		}
		return v;
	}
}
