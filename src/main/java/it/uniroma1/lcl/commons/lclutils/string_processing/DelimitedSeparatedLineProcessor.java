package it.uniroma1.lcl.commons.lclutils.string_processing;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tommaso on 07/10/16.
 */
public class DelimitedSeparatedLineProcessor implements LineProcessor<List<String>>
{
	private String delimiter;

	public DelimitedSeparatedLineProcessor(String delimiter)
	{
		this.delimiter = delimiter;
	}


	@Override
	public List<String> processLine(String line)
	{
		String[] split = line.split(delimiter);
		return Arrays.asList(split);
	}
}
