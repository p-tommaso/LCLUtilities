package it.uniroma1.lcl.commons.lclutils.babelnet;

import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.babelnet.BabelSynsetID;
import it.uniroma1.lcl.babelnet.BabelSynsetType;
import it.uniroma1.lcl.babelnet.data.BabelPOS;
import it.uniroma1.lcl.babelnet.data.BabelPointer;
import it.uniroma1.lcl.babelnet.resources.WordNetSynsetID;
import it.uniroma1.lcl.jlt.util.Strings;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by tommaso on 03/10/16.
 */
public class BabelnetUtils
{
	public static Integer bnIdStringToIntegerBnId(String bnId)
	{
		return Integer.parseInt(bnId.substring(3, bnId.length() - 1));
	}

	public static BabelPOS fromUniversalToBabelPos(String postag)
	{
		switch (postag)
		{
			case "ADJ":
				return BabelPOS.ADJECTIVE;
			case "ADV":
				return BabelPOS.ADVERB;
			case "INTJ":
				return BabelPOS.INTERJECTION;
			case "NOUN":
				return BabelPOS.NOUN;
			case "PROPN":
				return BabelPOS.NOUN;
			case "AUX":
				return BabelPOS.VERB;
			case "VERB":
				return BabelPOS.VERB;
			case "ADP":
				return BabelPOS.DETERMINER;
			case "DET":
				return BabelPOS.DETERMINER;
			case "CCONJ":
				return BabelPOS.CONJUNCTION;
			case "SCONJ":
				return BabelPOS.CONJUNCTION;
			case "PRON":
				return BabelPOS.PRONOUN;
			default:
				return null;
		}
	}

	public static BabelPOS fromPennTreeBankToBabelPos(String postag)
	{
		char c = postag.toLowerCase().charAt(0);
		if (c == 'j')
			c = 'a';

		return BabelPOS.valueOf(c);
	}

//	public static List<String> fromBabelPosToPennTreeBank(BabelPOS pos)
//	{
//		switch(pos)
//		{
//			case ADJECTIVE:
//				return Arrays.asList("JJ", "JJR", "JJS");
//			case NOUN:
//				return Arrays.asList("NN", "NNS", "NNP", "NNPS");
//			case ADVERB:
//				return Arrays.asList("RB", "RBR", "RBS");
//			case VERB:
//				return Arrays.asList("VB", "VBD", )
//		}
//
//	}


	public static String toExtendedBabelnetId(Integer id, char pos)
	{
		String sid = id + "";
		String filledId = Strings.leftFillWith(sid, '0', 8);
		return "bn:" + filledId + pos;
	}

	public static BabelSynset getSynsetFromIntegerID(Integer id)
	{
		BabelPOS[] values = BabelPOS.values();
		String filledId = "bn:" + Strings.leftFillWith(id + "", '0', 8);
		BabelSynset synset = null;
		for (BabelPOS pos : values)
		{
			try
			{
				synset = BabelNet.getInstance().getSynset(new BabelSynsetID(filledId + pos.getTag()));
				if (synset != null)
					break;
			} catch (Exception e)
			{
				e.printStackTrace();
			}

		}
		return synset;
	}

	public static Set<BabelPointer> getWordnetPointers()
	{
		Set<String> wnPointers = new HashSet<>(Arrays.asList("!", "@", "@i", "~", "~i", "#m", "#s",
				"#p", "%m", "%s", "%p", "=", "+", "*", ">", "^", "$", "\\", "<", "&", ";c", "-c", ";r", "-r", ";u", "-u"));
		return wnPointers.stream().map(BabelPointer::getPointerType).collect(Collectors.toSet());
	}

	public static void main(String[] args)
	{
		System.out.println(getWordnetPointers());
	}
}
