package it.uniroma1.lcl.commons.lclutils.string_processing;


import it.uniroma1.lcl.jlt.util.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

/**
 * Created by tommaso on 07/10/16.
 */
public class DelimitedLineWithDelimitedFieldProcessor<T,V> implements LineProcessor<List<Pair<T, V>>>
{
	private String fieldDelimiter;
	private String componentDelimiter;
	private Function<String, T> fieldFirstComponentTransformer;
	private Function<String, V> fieldSecondComponentTransformer;

	public DelimitedLineWithDelimitedFieldProcessor(String fieldDelimiter, String componentDelimiter, Function<String, T> fieldFirstComponentTransformer, Function<String, V> fieldSecondComponentTransformer)
	{
		this.fieldDelimiter = fieldDelimiter;
		this.componentDelimiter = componentDelimiter;
		this.fieldFirstComponentTransformer = fieldFirstComponentTransformer;
		this.fieldSecondComponentTransformer = fieldSecondComponentTransformer;
	}

	@Override
	public List<Pair<T, V>> processLine(String line)
	{
		List<Pair<T, V>> list = new LinkedList<>();
		String[] fields = line.split(fieldDelimiter);
		for(String field: fields)
		{
			String[] compoenents = field.split(componentDelimiter);
			T first = fieldFirstComponentTransformer.apply(compoenents[0]);
			V second = fieldSecondComponentTransformer.apply(compoenents[1]);
			list.add(new Pair<>(first, second));
		}
		return list;
	}
}
