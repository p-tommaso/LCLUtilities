package it.uniroma1.lcl.commons.lclutils.misc;

import it.uniroma1.lcl.commons.lclutils.string_processing.LineProcessor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

/**
 * Created by tommaso on 22/05/17.
 */
public class FileLines<T> implements Iterable<T>
{
	private String file;
	private LineProcessor<T> lineProcessor;

	public FileLines(String file, LineProcessor<T> processor) throws FileNotFoundException
	{
		this.file = file;
		this.lineProcessor = processor;
	}

	private class LineTransformerIterator implements Iterator<T>
	{
		private BufferedReader reader;
		private String currentLine;
		public LineTransformerIterator()
		{
			try
			{
				reader = new BufferedReader(new FileReader(file));
				currentLine = reader.readLine();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		@Override
		public boolean hasNext()
		{
			return currentLine != null;
		}

		@Override
		public T next()
		{
			if (hasNext())
			{
				try
				{
					String oldLine = currentLine;
					currentLine = reader.readLine();
					return lineProcessor.processLine(oldLine);
				} catch (IOException e)
				{
					e.printStackTrace();
					return null;
				}
			}
			throw new NoSuchElementException();

		}
	}

	@Override
	public Iterator<T> iterator()
	{
		return new LineTransformerIterator();
	}
}/*

//angstrom        12
ångström        13
angström        14
angstrom
*/
