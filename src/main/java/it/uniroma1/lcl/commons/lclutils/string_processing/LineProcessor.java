package it.uniroma1.lcl.commons.lclutils.string_processing;

/**
 * Created by tommaso on 07/10/16.
 */
@FunctionalInterface
public interface LineProcessor<T>
{
	T processLine(String line);
}
