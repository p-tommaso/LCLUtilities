package it.uniroma1.lcl.commons.lclutils.misc;

import java.io.PrintStream;
import java.util.*;
import java.util.function.Supplier;

/**
 * Created by tommaso on 08/05/17.
 */
public class IterableProgressBar<T> extends ProgressBar implements Iterable<T>
{
	private Iterator<T> iterator;

	private IterableProgressBar(Iterable<T> iterable, int total, int barLenght, String barChar, boolean enableEta, boolean enableDate, PrintStream printStream, String barName, Supplier<String> supplier)
	{
		super(total, barLenght, barChar, enableEta, enableDate, printStream, barName, supplier);
		this.iterator = iterable.iterator();
	}


	public static class IterableProgressBarBuilder<T> extends ProgressBarBuilder<IterableProgressBarBuilder<T>>
	{
		private Iterable<T> iterable;

		public IterableProgressBarBuilder(Iterable<T> iterable)
		{
			this.iterable = iterable;
			if(iterable instanceof Collection)
				setTotal(((Collection)iterable).size());
		}


		@Override
		public IterableProgressBar<T> build()
		{
			return new IterableProgressBar<>(iterable, total, barLenght, barChar, enableEta, enableDate, printStream, barName, supplier);
		}

		@Override
		protected IterableProgressBarBuilder<T> getThis()
		{
			return this;
		}
	}

	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>()
		{
			@Override
			public boolean hasNext()
			{
				return iterator.hasNext();
			}

			@Override
			public T next()
			{
				tick();
				return iterator.next();
			}
		};
	}



	public static void main(String[] args) throws InterruptedException
	{
		List<Integer> list = new ArrayList<>();

		for(int i = 0; i < 1000000; i++)
			list.add(i);

		IterableProgressBarBuilder<Integer> integerIterableProgressBarBuilder = new IterableProgressBarBuilder<>(list);
		IterableProgressBar<Integer> progressBar = integerIterableProgressBarBuilder.enableEta().build();

		int sum = 0;
		for(Integer in : integerIterableProgressBarBuilder.enableEta().build())
		{
			sum += in;
			Thread.sleep(10);
//			System.out.println(in);
		}


	}
}
