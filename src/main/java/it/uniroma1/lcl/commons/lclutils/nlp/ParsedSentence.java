package it.uniroma1.lcl.commons.lclutils.nlp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tommaso on 09/02/17.
 */
public class ParsedSentence
{
	public static class SentenceWord
	{
		private String word;
		private String lemma;
		private String tag;
		private int index;

		public SentenceWord(String word, String lemma, String tag, int index)
		{
			this.word = word;
			this.lemma = lemma;
			this.tag = tag;
			this.index = index;
		}


		public int getIndex()
		{
			return index;
		}

		public String getWord()
		{
			return word;
		}

		public String getLemma()
		{
			return lemma;
		}

		public String getTag()
		{
			return tag;
		}
	}

	private int id;
	private String rawSentence;
	private List<SentenceWord> words;

	public ParsedSentence(int id, String rawSentence, List<SentenceWord> words)
	{
		this.id = id;
		this.rawSentence = rawSentence;
		this.words = words;
	}

	public int getId()
	{
		return id;
	}

	public List<SentenceWord> getWords()
	{
		return words;
	}

	public String getRawSentence()
	{
		return rawSentence;
	}

	public static ParsedSentence fromLine(String line, String idSeparator, String tokenSeparator, String wltSeparator)
	{
		String[] split = line.split(idSeparator);
		int id = Integer.parseInt(split[0]);
		String rawSentence = split[1];
		String[] tokens = rawSentence.split(tokenSeparator);
		List<SentenceWord> words = new ArrayList<>();

		for(int i = 0; i < tokens.length; i++)
		{
			String token = tokens[i];
			String[] wlt = token.split(wltSeparator);
			words.add(new SentenceWord(wlt[0], wlt[1], wlt[2], i));
		}
		return new ParsedSentence(id, line, words);
	}
}
