package it.uniroma1.lcl.commons.lclutils.vectors;

import it.uniroma1.lcl.commons.lclutils.misc.ProgressBar;
import it.uniroma1.lcl.commons.lclutils.string_processing.LineProcessor;
import it.uniroma1.lcl.commons.lclutils.string_processing.VectorMapLineProcessor;
import it.uniroma1.lcl.jlt.util.DoubleCounter;
import it.uniroma1.lcl.jlt.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by tommaso on 27/04/17.
 * K is the type that indicize the vectors
 * C is the type of each component
 */
public class VectorSpace<K, C>
{
	private Map<K, DoubleCounter<C>> vectorSpace;

	private VectorSpace()
	{
		vectorSpace = new HashMap<>();
	}

	private void put(K key, DoubleCounter<C> vector)
	{
		vectorSpace.put(key, vector);

	}

	public DoubleCounter<C> getVector(K key)
	{
		return vectorSpace.get(key);
	}

	public static <K, C> VectorSpace<K, C>  loadVectorSpace(String file, VectorMapLineProcessor<K, C> processor, Set<K> keysToLoad)
	{
		VectorSpace<K, C> vectorSpace = new VectorSpace<>();
		ProgressBar bar = new ProgressBar.ProgressBarBuilder().enableDate().build();
		try(BufferedReader reader = new BufferedReader(new FileReader(file)))
		{
			String line = "";
			while((line = reader.readLine()) != null)
			{
				bar.tick();
				Pair<K, DoubleCounter<C>> pair = processor.processLine(line);
				if(keysToLoad == null || keysToLoad.contains(pair.getFirst()))
					vectorSpace.put(pair.getFirst(), pair.getSecond());
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return vectorSpace;
	}

	public static <K, C> VectorSpace<K, C>  loadVectorSpace(String file, VectorMapLineProcessor<K, C> processor)
	{
		return loadVectorSpace(file, processor, (HashSet<K>)null);
	}

	public static <K, C> VectorSpace<K, C>  loadVectorSpace(String file, VectorMapLineProcessor<K, C> processor, K... keysToLoad)
	{
		return loadVectorSpace(file, processor, new HashSet<>(Arrays.asList(keysToLoad)));
	}

}
