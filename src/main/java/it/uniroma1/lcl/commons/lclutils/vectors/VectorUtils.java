package it.uniroma1.lcl.commons.lclutils.vectors;

import it.uniroma1.lcl.commons.lclutils.string_processing.VectorMapLineProcessor;
import it.uniroma1.lcl.jlt.util.DoubleCounter;
import it.uniroma1.lcl.jlt.util.Pair;
import it.uniroma1.lcl.jlt.util.condition.IntegerCondition;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Created by tommaso on 07/10/16.
 */
public class VectorUtils
{
	public static <T, V> Map<T, DoubleCounter<V>> loadVectors(String file,
															  String fieldDelimiter,
															  String componentDelimiter,
															  Function<String, T> keyTranformer,
															  Function<String, V> componentTransformer)
	{
		return loadVectors(file, fieldDelimiter, componentDelimiter, -1, keyTranformer, componentTransformer);
	}

	public static <T, V> Map<T, DoubleCounter<V>> loadVectors(String file,
															  String fieldDelimiter,
															  String componentDelimiter,
															  int numOfDimensions,
															  Function<String, T> keyTranformer,
															  Function<String, V> componentTransformer)
	{
		Map<T, DoubleCounter<V>> map = new ConcurrentHashMap<>();
		VectorMapLineProcessor<T, V> lineProcessor = new VectorMapLineProcessor<T, V>(fieldDelimiter, componentDelimiter, numOfDimensions, keyTranformer, componentTransformer);
		IntegerCondition condition = new IntegerCondition(10000, VectorUtils.class.getName());

		try
		{
			Files.lines(Paths.get(file)).parallel().forEach(line ->
			{
				condition.triggerAndIncrement();
				Pair<T, DoubleCounter<V>> pair = lineProcessor.processLine(line);
				map.put(pair.getFirst(), pair.getSecond());
			});
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return map;
	}
}
