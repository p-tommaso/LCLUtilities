package it.uniroma1.lcl.commons.lclutils.nasari;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import edu.stanford.nlp.util.Sets;
import it.uniroma1.lcl.jlt.util.IntegerCounter;
import it.uniroma1.lcl.jlt.util.condition.IntegerCondition;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;

/**
 * Created by tommaso on 03/10/16.
 */
public class NASARIUtils
{

	public static final String NASARI_UNIFIED_DEFAULT_PATH = "/home/tommaso/Documents/data/nasari/unified/NASARI_unified_english.txt";
	public static final String NASARI_LEXICALIZED_DEFAULT_PATH = "/home/tommaso/Documents/data/nasari/lexical/NASARI_lexical_english.txt";

	private static Logger logger = Logger.getLogger(NASARIUtils.class);
/*	public static <T> Map<T, NasariVector<T>> loadVectors(String nasariPath, Function<String, T> bnIdConvertionFunction, Set<T> idsToLoad) throws IOException
	{
		Map<T, NasariVector<T>> nasariVectors = new HashMap<>();
		try(BufferedReader reader = Files.newBufferedReader(Paths.get(nasariPath)))
		{
			String line = null;
			while((line = reader.readLine()) != null)
			{
				String[] split = line.split("\t");
				String srcId = split[0];
				List<T> vector = new LinkedList<T>();
				for(int i = 2; i < split.length; i++)
				{
					String strId = split[i].split("_")[0];
					vector.add(bnIdConvertionFunction.apply(strId));
				}
				nasariVectors.put(bnIdConvertionFunction.apply(srcId), NasariVector.simpleVectorFromList(vector));
			}
		}
		return nasariVectors;
	}
*/
	public static <T> Map<T, NasariVector<T>> loadVectors(String nasariPath, Function<String, T> bnIdConvertionFunction, boolean skitpNASynsets) throws IOException
	{
		logger.info("loading nasari vectors");
		Map<T, NasariVector<T>> nasariVectors = new HashMap<>();
		try(BufferedReader reader = Files.newBufferedReader(Paths.get(nasariPath)))
		{
			String line = null;
			while((line = reader.readLine()) != null)
			{
				String[] split = line.split("\t");
				String wikiPage = split[1];
				if(skitpNASynsets && wikiPage.equals("-NA-"))
					continue;
				String srcId = split[0];
				List<T> vector = new LinkedList<T>();
				for(int i = 2; i < split.length; i++)
				{
					String strId = split[i].split("_")[0];
					vector.add(bnIdConvertionFunction.apply(strId));
				}
				nasariVectors.put(bnIdConvertionFunction.apply(srcId), NasariVector.simpleVectorFromList(vector));
			}
		}
		logger.info("loaded " + nasariVectors.size() + " vectors");
		return nasariVectors;
	}


	public static <T> Map<T, NasariVector<T>> loadVectors(String nasariPath, Function<String, T> bnIdConvertionFunction) throws IOException
	{
		return loadVectors(nasariPath, bnIdConvertionFunction, false);
	}



	public static <T> double weightedOverlap(NasariVector<T> a, NasariVector<T> b)
	{
		IntegerCounter<T> aPositions = new IntegerCounter<>();
		for(int i = 1; i <= a.getVector().size(); i++)
			aPositions.count(a.getComponent(i - 1), i);
		IntegerCounter<T> bPositions = new IntegerCounter<>();
		for(int i = 1; i <= b.getVector().size(); i++)
			bPositions.count(b.getComponent(i - 1), i);
		Set<T> auxSet = Sets.intersection(aPositions.keySet(), bPositions.keySet());
		TreeSet<T> overlap = new TreeSet<>((z,x) ->
				Integer.compare(aPositions.get(z), aPositions.get(x)));
		overlap.addAll(auxSet);
		if(overlap.isEmpty())
			return 0.0;
		double sum = 0.0;
		double den = 0.0;
		int i = 1;
		for(T o : overlap)
		{
			double arank = (double)aPositions.get(o);
			double brank = (double)bPositions.get(o);
			sum += 1.0 / (arank + brank);
			den += 1.0 / (2.0 * i);
			i++;
		}
		return sum / den;
	}

	public static void main(String[] args) throws IOException
	{
		Map<Integer, NasariVector<Integer>> vectors = NASARIUtils.loadVectors(NASARIUtils.NASARI_UNIFIED_DEFAULT_PATH, x -> Integer.parseInt(x.substring(3, x.length() - 1)));
		Scanner scan = new Scanner(System.in);
		while(true)
		{
			String line = scan.nextLine();
			if(line.equalsIgnoreCase("exit") || line.equalsIgnoreCase("quit"))
				break;
			String[] fields = line.split("\\s+");
			NasariVector<Integer> first = vectors.get(Integer.parseInt(fields[0]));
			NasariVector<Integer> second = vectors.get(Integer.parseInt(fields[1]));
			double v = NASARIUtils.weightedOverlap(first, second);
			System.out.println(v);
		}
		System.out.println("done");
	}
}
