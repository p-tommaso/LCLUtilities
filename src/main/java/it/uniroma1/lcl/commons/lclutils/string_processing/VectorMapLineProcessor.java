package it.uniroma1.lcl.commons.lclutils.string_processing;

import it.uniroma1.lcl.jlt.util.DoubleCounter;
import it.uniroma1.lcl.jlt.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.function.Function;

/**
 * Created by tommaso on 07/10/16.
 */
public class VectorMapLineProcessor<K, T> implements LineProcessor<Pair<K, DoubleCounter<T>>>
{
	private String fieldDelimiter;
	private String componentDelimiter;
	private Function<String, K> keyTransformer;
	private Function<String, T> componentTransformer;
	private int dimensionLimit;
	private Logger logger = LoggerFactory.getLogger(VectorMapLineProcessor.class);

	public VectorMapLineProcessor(Function<String, K> keyTransformer, Function<String, T> componentTransformer)
	{
		this("\t", ",", keyTransformer, componentTransformer);
	}

	public VectorMapLineProcessor(String fieldDelimiter, String componentDelimiter, Function<String, K> keyTransformer,
	                              Function<String, T> componentTransformer)
	{
		this(fieldDelimiter, componentDelimiter, -1, keyTransformer, componentTransformer);
	}

	public VectorMapLineProcessor(String fieldDelimiter, String componentDelimiter, int dimensionLimit, Function<String, K> keyTransformer, Function<String, T> componentTransformer)
	{
		this.fieldDelimiter = fieldDelimiter;
		this.componentDelimiter = componentDelimiter;
		this.keyTransformer = keyTransformer;
		this.componentTransformer = componentTransformer;
		this.dimensionLimit = dimensionLimit;
	}

	public void setDimensionLimit(int dimensionLimit)
	{
		this.dimensionLimit = dimensionLimit;
	}


	@Override
	public Pair<K, DoubleCounter<T>> processLine(String line)
	{
		String[] fields = line.split(fieldDelimiter);
		K key = null;
		try
		{
			key = keyTransformer.apply(fields[0]);
		}catch(Exception e)
		{
			logger.error("Unable to parse " + fields[0] + " with the provided key transformation function");
			throw e;
		}
		DoubleCounter<T> counter = new DoubleCounter<T>();
		int limit = dimensionLimit > 0? Math.min(dimensionLimit, fields.length - 1) : fields.length - 1;
		for(int i = 1; i <= limit; i++)
		{
			String[] components = fields[i].split(componentDelimiter);
			T compKey = null;
			try
			{
				compKey = componentTransformer.apply(components[0]);
			}catch(Exception e)
			{
				logger.error("Unable to parse " + components[0] + " in " + fields[i] + " with the provided component transformation function");
			}
			Double value = Double.parseDouble(components[1]);
			counter.count(compKey, value);
		}
		return new Pair<K, DoubleCounter<T>>(key, counter);
	}
}
