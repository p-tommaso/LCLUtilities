package it.uniroma1.lcl.commons.lclutils.misc;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.io.PrintStream;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * Created by tommaso on 25/10/16.
 */
public class ProgressBar
{
	private AtomicInteger barLenght;
	private AtomicInteger total;
	private AtomicInteger progress;
	private AtomicInteger tick;
	private String barName;
	private StringBuilder progressBar;
	private PrintStream printStream;
	private String barChar;
	private DateTime startTime;
	private Period ETA;
	private PeriodFormatter periodFormatter;
	private Supplier<String> additionalInfo;
	private int prefixLength;
	private boolean enableETA;
	private boolean enableDate;
	private DateTime lastUpdate;
	private static final int DEFAULT_BAR_LEGHT = 100;
	private static final PrintStream DEFAULT_PRINT_STREAM = System.out;
	private static final String DEFAULT_BAR_CHAR = "=";
	private static final String DEFAULT_BAR_NAME = "progress-bar";
	private static final Supplier<String> DEFAULT_SUPPLIER = null;

	public static class ProgressBarBuilder <T extends ProgressBarBuilder>
	{
		int total;
		int barLenght;
		PrintStream printStream;
		String barChar;
		boolean enableEta;
		String barName;
		boolean enableDate;
		Supplier<String> supplier;

		public ProgressBarBuilder()
		{
			this(-1);
		}

		public ProgressBarBuilder(int total)
		{
			this.total = total;
			this.barLenght = DEFAULT_BAR_LEGHT;
			this.printStream = DEFAULT_PRINT_STREAM;
			this.barChar = DEFAULT_BAR_CHAR;
			this.enableEta = false;
			this.enableDate = false;
			this.barName = DEFAULT_BAR_NAME;
			this.supplier = DEFAULT_SUPPLIER;
		}

		protected T getThis()
		{
			return (T) this;
		}

		public T setAdditionalInfo(Supplier<String> supplier)
		{
			this.supplier = supplier;
			return (T) this;
		}

		public T enableEta()
		{
			this.enableEta = true;
			return (T) getThis();
		}

		public T enableDate()
		{
			this.enableDate = true;
			return (T) getThis();

		}

		public T disableEta()
		{
			this.enableEta = false;
			return (T) getThis();
		}

		public T withName(String name)
		{
			this.barName = name;
			return (T) getThis();
		}

		public T withBarLength(int barLength)
		{
			this.barLenght = barLength;
			return (T) getThis();
		}

		public T withOutStream(PrintStream stream)
		{
			this.printStream = stream;
			return (T) getThis();
		}

		public T withCharacter(String barChar)
		{
			this.barChar = barChar;
			return (T) getThis();
		}

		public  T setTotal(int total)
		{
			this.total = total;
			return (T) getThis();
		}

		public <E extends ProgressBar> E build()
		{
			return (E)new ProgressBar(total, barLenght, barChar, enableEta, enableDate, printStream, barName, supplier);
		}


	}

	public ProgressBar(int total)
	{
		this(total, 100);
	}

	public ProgressBar(int total, int barLenght)
	{
		this(total, barLenght, "=");
	}

	public ProgressBar(int total, int barLenght, String barChar)
	{
		this(total, barLenght, barChar, System.out);
	}

	public ProgressBar(int total, int barLenght, String barChar, PrintStream stream)
	{
		this(total, barLenght, barChar, false, stream);
	}

	public ProgressBar(int total, int barLenght, String barChar, boolean enableETA, PrintStream stream)
	{
		this(total, barLenght, barChar, enableETA, false, stream, DEFAULT_BAR_NAME, () -> "");
	}

	public ProgressBar(int total, int barLenght, String barChar, boolean enableETA, boolean enableDate, PrintStream stream, String name, Supplier<String> additionalInfo)
	{
		this.barLenght = new AtomicInteger(barLenght);
		this.total = new AtomicInteger(total);
		this.enableDate = enableDate;
		this.barChar = barChar;
		this.printStream = stream;
		this.progress = new AtomicInteger(0);
		this.tick = new AtomicInteger(0);
		this.enableETA = enableETA;
		this.barName = name;
		this.progressBar = new StringBuilder(barName).append(": [");
		this.prefixLength = progressBar.length();
		for (int i = 0; i < barLenght; i++)
			this.progressBar.append(" ");
		this.progressBar.append("]").append("[0/").append(total).append("]");
		this.startTime = new DateTime(System.currentTimeMillis());
		this.periodFormatter = new PeriodFormatterBuilder()
				.appendHours().appendSuffix("h ")
				.appendMinutes().appendSuffix("m ")
				.appendSeconds().appendSuffix("s")
				.printZeroNever()
				.toFormatter();
		this.lastUpdate = null;
		this.additionalInfo = additionalInfo;
		printProgressBar();
	}


	private void tickNoTotal()
	{
		tick.incrementAndGet();
		synchronized (progressBar)
		{
			int endBar = progressBar.indexOf("]");
			progressBar.delete(endBar + 1, progressBar.length());

			progressBar.append("[").append(tick).append("/").append(total).append("]");
		}
		printProgressBar();

	}


	public int getTick()
	{
		return tick.get();
	}

	public void tick()
	{
		if (total.get() > 0 && tick.get() >= total.get())
		{
//			throw new IllegalStateException("The progress bar reached the end. Cannot increment the status. " +
//					"Did you set the total to a value lower than the real total?");
//			System.err.println("");
			return; //FIXME sta porcata è per scozzafava ma va ripensata
		}
		tick.incrementAndGet();
		updateProgressBar();
		if (tick.get() == total.get())
			printStream.println();
	}

	private Period computeETA(Period elapsedTime)
	{
		if (enableETA)
		{
//			if (tick.get() % ((double)total.get() / 100) == 0) //se ho fatto il'10% degli step calcolo il tempo rimanente
			{
				Seconds seconds = elapsedTime.toStandardSeconds();
				double timePerStep = (seconds.getSeconds() * 1000.0) / (double) tick.get();
				int etaMillis = new Double(timePerStep * (total.get() - tick.get())).intValue();
				ETA = new Period(etaMillis);
			}
		}
		return ETA;
	}

	public void setBarName(String barName)
	{
		this.barName = barName;
	}

	private synchronized void updateProgressBar()
	{
		if (total.get() > 0)
		{
			int newProgress = (int) (tick.get() * barLenght.get() / (double) total.get());
			if (newProgress > progress.get())
			{
				progressBar.replace(prefixLength + progress.get(), prefixLength + progress.get()
						+ barChar.length(), barChar);
				progress.set(newProgress);
			}
		}

		Period elapsedTime = new Period(startTime, DateTime.now());
		ETA = computeETA(elapsedTime);

		String elapsedTimeStr = periodFormatter.print(elapsedTime);
		int endBar = progressBar.indexOf("]");
		progressBar.delete(endBar + 1, progressBar.length());
		progressBar.append("[").append(tick).append("/").append(total).append("]");

		if (enableDate)
			progressBar.append(" [").append(DateTimeFormat.forPattern("H:m:s - dd/MMM").print(startTime)).append("]");

		progressBar.append(" - ").append(elapsedTimeStr);


		if (ETA != null)
			progressBar.append(" ETA: ").append(periodFormatter.print(ETA));

		if (lastUpdate != null)
		{
			Period period = new Period(lastUpdate, DateTime.now());
			if (period.getSeconds() < 1 && tick.get() != total.get())
				return;
		}
		lastUpdate = DateTime.now();

		if (additionalInfo != null)
			progressBar.append("[").append(additionalInfo.get()).append("]");

		printProgressBar();
	}

	private void printProgressBar()
	{
		printStream.print("\r");
		printStream.print(progressBar.toString());
	}

	public static void main(String[] args) throws InterruptedException
	{
		int total = 800000;
		int barLength = 176;

		ProgressBar progressBar = new ProgressBarBuilder(total)
				.enableEta()
				.enableDate()
//				.withCharacter("*")
				.build();
		IntStream.range(0, total).parallel().forEach(i ->
		{
			progressBar.tick();
			try
			{
				Thread.sleep(1);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}

		});
	}
}
