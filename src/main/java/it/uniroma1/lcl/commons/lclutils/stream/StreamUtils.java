package it.uniroma1.lcl.commons.lclutils.stream;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by tommaso on 03/10/16.
 */
public class StreamUtils
{
	/**
	 *
	 * @param iterator iterator to convert to a stream
	 * @param <T> type of element on which iterator iterates
	 * @return a newly created stream which is immutable and sequencial
	 */
	public static <T> Stream<T> iteratorToStream(Iterator<T> iterator)
	{
		Stream<T> stream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.IMMUTABLE), false);
		return stream;
	}


	/**
	 *
	 * @param iterator iterator to convert to a stream
	 * @param <T> type of element on which iterator iterates
	 * @return a newly created stream which is immutable and parallel
	 */
	public static <T> Stream<T> iteratorToParallelStream(Iterator<T> iterator)
	{
		return iteratorToStream(iterator).parallel();
	}
}
