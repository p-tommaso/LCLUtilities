package it.uniroma1.lcl.commons.lclutils.string_processing;


import it.uniroma1.lcl.jlt.util.Pair;

import java.util.function.Function;

/**
 * Created by tommaso on 10/10/16.
 */
public class PairLineProcessor<T, V> implements LineProcessor<Pair<T,V>>
{
	private Function<String, T> keyTransformer;
	private Function<String, V> valueTransformer;
	private String delimiter;

	public PairLineProcessor(Function<String, T> keyTransformer, Function<String, V> valueTransformer, String delimiter)
	{
		this.keyTransformer = keyTransformer;
		this.valueTransformer = valueTransformer;
		this.delimiter = delimiter;
	}



	@Override
	public Pair<T, V> processLine(String line)
	{
		String[] split = line.split(delimiter);
		return new Pair<>(keyTransformer.apply(split[0]), valueTransformer.apply(split[1]));
	}
}
