package it.uniroma1.lcl.commons.lclutils.maps;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import it.uniroma1.lcl.commons.lclutils.babelnet.BabelnetUtils;
import it.uniroma1.lcl.commons.lclutils.string_processing.DelimitedSeparatedLineProcessor;
import it.uniroma1.lcl.commons.lclutils.string_processing.PairLineProcessor;
import it.uniroma1.lcl.jlt.util.Pair;
import it.uniroma1.lcl.jlt.util.condition.IntegerCondition;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by tommaso on 10/10/16.
 */
public class MapUtils
{
	private static Logger log = Logger.getLogger(MapUtils.class);

	public static <T, V> Map<T, V> readMap(String path, String delimiter, Function<String , T> keyTransformer,
										   Function<String, V> valueTransformer, Predicate<T> keyFilter,
										   Supplier<Map<T, V>> supplier) throws IOException
	{
		log.info("Reading map " + path);

		PairLineProcessor<T, V> lineProcessor = new PairLineProcessor<>(keyTransformer, valueTransformer, delimiter);
		Map<T, V> map = supplier.get();
		int i = 0;
		IntegerCondition condition = new IntegerCondition(100000, MapUtils.class.getName());
		try(BufferedReader reader = Files.newBufferedReader(Paths.get(path)))
		{
			while(reader.ready())
			{
//				condition.triggerAndIncrement();
				Pair<T, V> tvPair = lineProcessor.processLine(reader.readLine());
				if(keyFilter.test(tvPair.getFirst()))
					map.put(tvPair.getFirst(), tvPair.getSecond());
			}
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		return map;
	}

	public static <T, V> Map<T, V> readMap(String path, String delimiter, Function<String , T> keyTransformer,
										   Function<String, V> valueTransformer, Predicate<T> keyFilter) throws IOException
	{
		return readMap(path, delimiter, keyTransformer, valueTransformer, keyFilter, HashMap::new);
	}

	public static <T, V> Map<T, V> readMap(String path, String delimiter, Function<String , T> keyTransformer, Function<String, V> valueTransformer) throws IOException
	{
		return readMap(path, delimiter, keyTransformer, valueTransformer, x -> true);
	}

	public static <V, T> Map<V, T> invertMap(Map<T, V> map)
	{
		Map<V, T> invertedMap = new HashMap<>();
		map.entrySet().forEach(entry -> invertedMap.put(entry.getValue(), entry.getKey()));
		return invertedMap;
	}

	public static <V, T> Multimap<V, T> invertMap(Multimap<T, V> multimap)
	{
		Multimap<V, T> invertedMap = HashMultimap.create();
		for(T k : multimap.keySet())
		{
			Collection<V> vs = multimap.get(k);
			for(V v : vs)
				invertedMap.put(v, k);
		}
		return invertedMap;
	}

	public static <T, V> Multimap<T, V> readMultimap(String path, String delimiter, Function<String, T> keyTransformer, Function<String, V> valueTransformer, Predicate<T> keyFilter, Integer... projection)
	{
		if(projection != null && projection.length == 1)
			throw new IllegalArgumentException("Cannot project map in less than 2 columns. Check projection argument");
		log.info("Reading map " + path);
		DelimitedSeparatedLineProcessor lineProcessor = new DelimitedSeparatedLineProcessor(delimiter);
		Multimap<T, V> multimap = HashMultimap.create();
		try(BufferedReader reader = Files.newBufferedReader(Paths.get(path)))
		{
			while(reader.ready())
			{
				String line = reader.readLine();
				if(line.isEmpty())
					continue;
				List<String> fields = lineProcessor.processLine(line);
				fields = projectFields(fields, projection);
				T key = keyTransformer.apply(fields.get(0));
				if(keyFilter.test(key))
				{
					List<V> values = new LinkedList<V>();
					for(int i = 1; i < fields.size(); i++)
					{
						String strVal = fields.get(i);
						V val = valueTransformer.apply(strVal);
						values.add(val);
					}
					multimap.putAll(key, values);
				}
			}
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		return multimap;
	}

	private static <V> List<V> projectFields(List<V> values, Integer[] projection)
	{
		List<V> projValues = new LinkedList<V>();
		if(projection == null || projection.length == 0)
			return values;
		for(int i = 0; i < projection.length; i++)
		{
			if(projection[i]  >= values.size())
				throw new IllegalArgumentException("the projection index " + projection[i] + " is greater or equal to the fields size in the file.");

			projValues.add(values.get(projection[i]));
		}
		return projValues;
	}

	public static <T, V> Multimap<T, V> readMultimap(String path, String delimiter, Function<String, T> keyTransformer, Function<String, V> valueTransformer, Integer... projection)
	{
		return readMultimap(path, delimiter, keyTransformer, valueTransformer, x -> true, projection);
	}


	public static <T, V> Multimap<T, V> readMultimap(String path, String delimiter, Function<String, T> keyTransformer, Function<String, V> valueTransformer)
	{
		return readMultimap(path, delimiter, keyTransformer, valueTransformer, x -> true);
	}

	public static <T,V> void writeMultimap(String path, Multimap<T, V> multimap, String keyValueSeparator, String valuesSeparator) throws IOException
	{
		try(BufferedWriter writer = Files.newBufferedWriter(Paths.get(path)))
		{
			for(T key : multimap.keySet())
			{
				Collection<V> vs = multimap.get(key);
				String values = vs.stream().map(V::toString).collect(Collectors.joining(valuesSeparator));
				writer.write(key.toString() + keyValueSeparator + values + "\n");
			}
		}catch(IOException e)
		{
			throw e;
		}

	}

	public static class MapsBuilder<K, V>
	{
		private Map<K, V> map;

		public static class MapEntry<A, B>
		{
			private A key;
			private B value;

			public MapEntry(A key, B value)
			{
				this.key = key;
				this.value = value;
			}

			public B getValue()
			{
				return value;
			}


			public A getKey()
			{
				return key;
			}
		}

		public MapsBuilder()
		{
			this(HashMap::new);
		}

		public MapsBuilder(Supplier<Map<K, V>> mapSupplier)
		{
			map = mapSupplier.get();
		}

		public Map<K, V> get()
		{
			return map;
		}

		public MapsBuilder put(K key, V value)
		{
			map.put(key, value);
			return this;
		}


		public static <A,B>  Map<A, B> getMap(MapEntry<A,B>... entries)
		{
			return getMap(HashMap::new, entries);
		}

		public static <A,B>  Map<A, B> getMap(Supplier<Map<A, B>> supplier, MapEntry<A,B>... entries)
		{
			Map<A,B> map = supplier.get();
			for(MapEntry<A,B> me : entries)

				map.put(me.getKey(), me.getValue());
			return map;
		}
	}


	public static void main(String[] args)
	{
		Multimap<Integer, String> multimap = readMultimap("/media/tommaso/4940d845-c3f3-4f0b-8985-f91a0b453b07/factories/output/synsetToSenses.txt", "\t",
				BabelnetUtils::bnIdStringToIntegerBnId, s -> s.substring(0, s.length() -2), 0,1,3);
		multimap.entries().stream().limit(10).forEach(x -> System.out.println(x));
	}

}
