package it.uniroma1.lcl.commons.lclutils.distributions;

import it.uniroma1.lcl.jlt.util.DoubleCounter;

import java.util.Collection;

/**
 * Created by tommaso on 03/10/16.
 */
public class DistributionUtils
{
	public static <T> DoubleCounter<T> uniformDistribution(Collection<T> elements)
	{
		double val = 1.0/elements.size();
		DoubleCounter<T> uniformDistribution = new DoubleCounter<>();
		elements.forEach(elem -> uniformDistribution.count(elem, val));
		return uniformDistribution;
	}

}
