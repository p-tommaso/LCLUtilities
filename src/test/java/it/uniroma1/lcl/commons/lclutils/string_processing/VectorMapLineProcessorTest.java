package it.uniroma1.lcl.commons.lclutils.string_processing;

import it.uniroma1.lcl.commons.lclutils.babelnet.BabelnetUtils;
import it.uniroma1.lcl.jlt.util.DoubleCounter;
import it.uniroma1.lcl.jlt.util.Pair;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by tommaso on 07/10/16.
 */
public class VectorMapLineProcessorTest
{
	@Test
	public void processLine() throws Exception
	{
		VectorMapLineProcessor<Integer, Integer> mapProcessor = new VectorMapLineProcessor<>("\t", ",",
				BabelnetUtils::bnIdStringToIntegerBnId, BabelnetUtils::bnIdStringToIntegerBnId);
		String testLine2 = "bn:00035478n\tbn:00035471n,0.003\tbn:00035472n,0.0234\tbn:00035473n,0.5\tbn:00035474n,0.9";
		Pair<Integer, DoubleCounter<Integer>> pair2 = mapProcessor.processLine(testLine2);
		assertTrue(pair2.getFirst() == 35478);
		DoubleCounter<Integer> values = pair2.getSecond();
		assertTrue(values.get(35471) == 0.003);
		assertTrue(values.get(35472) == 0.0234);
		assertTrue(values.get(35473) == 0.5);
		assertTrue(values.get(35474) == 0.9);
	}

}