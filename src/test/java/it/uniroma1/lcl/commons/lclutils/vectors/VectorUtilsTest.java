package it.uniroma1.lcl.commons.lclutils.vectors;

import it.uniroma1.lcl.commons.lclutils.babelnet.BabelnetUtils;
import it.uniroma1.lcl.jlt.util.DoubleCounter;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by tommaso on 07/10/16.
 */
public class VectorUtilsTest
{
	@Test
	public void loadLimitedDimensionVector() throws Exception
	{
		Map<Integer, DoubleCounter<Integer>> vectors = VectorUtils.loadVectors("src/test/resources/vectors.test.txt", "\t", ",", 2, BabelnetUtils::bnIdStringToIntegerBnId, BabelnetUtils::bnIdStringToIntegerBnId);
		DoubleCounter<Integer> v1 = vectors.get(1511);
		DoubleCounter<Integer> v2 = vectors.get(1512);
		assertNotNull(v1);
		assertNotNull(v2);
		assertTrue(v1.size() == 2);
		assertTrue(v2.size() == 2);
		Double v11 = v1.get(44551);
		Double v12 = v1.get(44552);
		assertEquals(0.08, v11, 0.0);
		assertEquals(0.2, v12, 0.0);

		Double v21 = v2.get(54551);
		Double v22 = v2.get(54552);
		assertEquals(0.08, v21, 0.0);
		assertEquals(0.2, v22, 0.0);
	}
	@Test
	public void loadVectors() throws Exception
	{
		Map<Integer, DoubleCounter<Integer>> vectors = VectorUtils.loadVectors("src/test/resources/vectors.test.txt", "\t", ",", BabelnetUtils::bnIdStringToIntegerBnId, BabelnetUtils::bnIdStringToIntegerBnId);
		DoubleCounter<Integer> v1 = vectors.get(1511);
		DoubleCounter<Integer> v2 = vectors.get(1512);
		assertNotNull(v1);
		assertNotNull(v2);

		Double v11 = v1.get(44551);
		Double v12 = v1.get(44552);
		Double v13 = v1.get(44553);
		Double v14 = v1.get(44554);
		assertEquals(0.08, v11, 0.0);
		assertEquals(0.2, v12, 0.0);
		assertEquals(0.02, v13, 0.0);
		assertEquals(0.7, v14, 0.0);

		Double v21 = v2.get(54551);
		Double v22 = v2.get(54552);
		Double v23 = v2.get(54553);
		Double v24 = v2.get(54554);
		Double v25 = v2.get(54555);
		assertEquals(0.08, v21, 0.0);
		assertEquals(0.2, v22, 0.0);
		assertEquals(0.02, v23, 0.0);
		assertEquals(0.7, v24, 0.0);
		assertEquals(0.1, v25, 0.0);

	}

}