package it.uniroma1.lcl.commons.lclutils.string_processing;

import it.uniroma1.lcl.commons.lclutils.babelnet.BabelnetUtils;
import it.uniroma1.lcl.jlt.util.DoubleCounter;
import it.uniroma1.lcl.jlt.util.Pair;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by tommaso on 07/10/16.
 */
public class DelimitedLineWithDelimitedFieldProcessorTest
{
	@Test
	public void processLine() throws Exception
	{
		DelimitedLineWithDelimitedFieldProcessor<Integer, Double> processor =
				new DelimitedLineWithDelimitedFieldProcessor<>("\t", ",", Integer::parseInt, Double::parseDouble);

		String testLine1 = "1,0.5\t2,0.3\t3,0.1\t4,0.05\t5,0.05";
		List<Pair<Integer, Double>> pairs = processor.processLine(testLine1);
		assertTrue(pairs.get(0).getFirst() == 1 && pairs.get(0).getSecond() == 0.5);
		assertTrue(pairs.get(1).getFirst() == 2 && pairs.get(1).getSecond() == 0.3);
		assertTrue(pairs.get(2).getFirst() == 3 && pairs.get(2).getSecond() == 0.1);
		assertTrue(pairs.get(3).getFirst() == 4 && pairs.get(3).getSecond() == 0.05);
		assertTrue(pairs.get(4).getFirst() == 5 && pairs.get(4).getSecond() == 0.05);
	}

}