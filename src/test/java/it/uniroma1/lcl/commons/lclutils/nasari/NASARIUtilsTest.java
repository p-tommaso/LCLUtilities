package it.uniroma1.lcl.commons.lclutils.nasari;

import it.uniroma1.lcl.commons.lclutils.babelnet.BabelnetUtils;
import it.uniroma1.lcl.jlt.jgrapht.WeightedLabeledEdge;
import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.hamcrest.number.OrderingComparison;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.results.ResultMatchers;
import org.junit.internal.runners.statements.Fail;
import org.junit.runner.notification.Failure;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by tommaso on 03/10/16.
 */
public class NASARIUtilsTest
{
	private static String NASARI_UNIFIED_PATH = "/home/tommaso/Documents/data/nasari/unified/NASARI_unified_english.txt";
	private static final int NASARI_SIZE = 2867355;
	private static final int MAX_LINES_TO_TEST = 100;
	private static Map<Integer, NasariVector<Integer>> NASARI_VECTORS;
	private static Logger logger = Logger.getLogger(NASARIUtilsTest.class);

	@BeforeClass
	public static void setUp()
	{
		logger.info("Loading nasari vectors");
		try
		{
			NASARI_VECTORS = NASARIUtils.loadVectors(NASARI_UNIFIED_PATH, BabelnetUtils::bnIdStringToIntegerBnId);
		} catch (IOException e)
		{
			fail(e.getMessage());
		}
		logger.info("vectors loaded");
	}

	@Test
	public void loadVectors() throws Exception
	{
		assertTrue(NASARI_VECTORS.size() == NASARI_SIZE);
		Random r = new Random();
		Set<Integer> randomValues = new HashSet<>();
		while (randomValues.size() < MAX_LINES_TO_TEST)
			randomValues.add(r.nextInt(NASARI_SIZE));
		int count = 0;
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(NASARI_UNIFIED_PATH)))
		{
			while (reader.ready())
			{
				String line = reader.readLine();
				count++;
				if (!randomValues.contains(count))
					continue;
				String[] fields = line.split("\t");
				Integer id = BabelnetUtils.bnIdStringToIntegerBnId(fields[0]);
				NasariVector<Integer> vector = NASARI_VECTORS.get(id);
				assertNotNull(vector);
				for (int i = 2; i < fields.length; i++)
				{
					String dst = fields[i].split("_")[0];
					Integer dstId = BabelnetUtils.bnIdStringToIntegerBnId(dst);
					Integer component = vector.getComponent(i - 2);
					assertEquals(dstId, component);
				}
				if (count == MAX_LINES_TO_TEST)
					break;
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public void weightedOverlap() throws Exception
	{
//		NasariVector<Integer> appleInc = NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:03739345n"));
//		NasariVector<Integer> appleFruit =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00005054n"));
//		NasariVector<Integer> computer =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00021464n"));

		NasariVector<Integer> flatApartment =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00004836n"));
		NasariVector<Integer> home =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00000356n"));
		NasariVector<Integer> flatMusic =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00035081n"));

		NasariVector<Integer> airplane =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00002275n"));
		NasariVector<Integer> planeAirplane =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00001697n"));
		NasariVector<Integer> planeGeometry =  NASARI_VECTORS.get(BabelnetUtils.bnIdStringToIntegerBnId("bn:00062766n"));

//		double appleInc_appleFruit = NASARIUtils.weightedOverlap(appleInc, appleFruit);
//		double appleInc_computer = NASARIUtils.weightedOverlap(appleInc, computer);

		assertThat(NASARIUtils.weightedOverlap(flatApartment, flatApartment), OrderingComparison.greaterThan(0.99));


		double flatApartment_home = NASARIUtils.weightedOverlap(flatApartment, home);
		double flatApartment_flatMusic = NASARIUtils.weightedOverlap(flatApartment, flatMusic);

		assertThat(flatApartment_home, OrderingComparison.greaterThan(flatApartment_flatMusic));

		double airplane_planeAirplane = NASARIUtils.weightedOverlap(airplane, planeAirplane);
		double airplane_planeGeometry = NASARIUtils.weightedOverlap(airplane, planeGeometry);

		assertThat(airplane_planeAirplane, OrderingComparison.greaterThan(airplane_planeGeometry));
	}
}